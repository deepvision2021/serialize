#include <memory>
#include "Serialize.h"

class A : public zdsj::SerializeAble
{
public:
    A()
    {
        
    }

    A(int a, int b):a(a), b(b)
    {
        
    }

    void show()
    {
        std::cout << "a=" << a << " b=" <<  b << std::endl;
    }

    SERIALIZEABLE(a, b)
private:
    int a = 2;
    float b = 23.0;
};

int main(int argc, char* argv[])
{
    // zdsj::Serialize* stream = new zdsj::Serialize();

    zdsj::Serialize stream;
    int64_t data = 6211;
    
    std::vector<int> v = {2};
    std::vector<int>::value_type vt;
    std::map<std::string, int> m;
    m.insert(std::pair<std::string, int>("1", 1));
    m.insert(std::pair<std::string, int>("2", 2));
    m.insert(std::pair<std::string, int>("3", 3));
    m.insert(std::pair<std::string, int>("4", 4));
    A a = A(5, 25);
    v.insert(v.end(), 5);
    std::list<int> l;
    l.reverse();
    std::string t = "33";
    
    stream << 300 << data << "22" << t << v << m << a;
    
    int i;
    int64_t rd;
    char* c = nullptr;
    std::string rt;
    std::vector<int> rv;
    std::map<std::string, int> rm;
    A b;
    std::string message = stream.toStream();
    std::cout << message.size() << std::endl;
    stream.clear();
    stream.load(message);
    stream >> i >> rd >> c >> rt >> rv >> rm >> b;
    std::cout << i << std::endl;

    std::cout << rd << std::endl;

    std::cout << c << std::endl;

    std::cout << rt << std::endl;

    b.show();

    stream.show();
    std::cout << stream.tell() << std::endl;
    
    // stream.clear();
    stream.show();
    delete[] c;

    return 0;
}


