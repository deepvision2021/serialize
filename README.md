# Serialize

#### 介绍
c++序列化

由于反序列化实现的方式，在使用时需要反序列化顺序与序列化相同，否则容易出现类型不匹配导致反序列化失败的情况。

代码实现主要在Serialize.h中

关于自定义类型，需要继承SerializeAble类并且调用宏SERIALIZEABLE将自定义类中需要序列化的字段写入。

```
class A : public zdsj::SerializeAble
{
public:
    A()
    {
        
    }

    void show()
    {
        std::cout << "a=" << a << " b=" <<  b << std::endl;
    }

    SERIALIZEABLE(a, b)
private:
    int a = 2;
    float b = 23.0;
};
```
